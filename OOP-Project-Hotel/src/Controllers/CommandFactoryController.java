package Controllers;

import Models.CommandModels.CommandInterface;
import Models.CommandModels.FileCommands.*;
import Models.CommandModels.HubCommands.*;
import Models.Hub;
import Views.RoomView;

/**
 * The {@code CommandFactoryController} class is responsible for parsing user input
 * and creating instances of command objects based on the provided input.
 *
 * <p>This class implements a factory design pattern to dynamically instantiate and return
 * the appropriate command object for execution. The available commands depend on whether a file
 * is currently open or not, as indicated by the {@code fileName} parameter.
 *
 * <p>It creates and returns instances of command classes such as {@link OpenCommand},
 * {@link CloseCommand}, {@link SaveCommand}, and others.
 */
public class CommandFactoryController {

    /**
     * Parses the given input and returns the corresponding {@link CommandInterface} object.
     *
     * <p>This method checks if a file is currently open by examining the {@code fileName} parameter.
     * If no file is open, it only allows certain commands (like "open" or "help") to be parsed.
     * Once a file is open, additional commands (like "save", "close", "checkin", etc.) are made available.
     *
     * @param givenInput the user input that represents a command and its arguments
     * @param fileName   the name of the currently open file, or {@code null} if no file is open
     * @param filePath   the path of the currently open file, or {@code null} if no file is open
     * @return an instance of the appropriate {@link CommandInterface} that corresponds to the input command,
     * or {@code null} if the command is not recognized or invalid
     */
    public CommandInterface parseCommand(String[] givenInput, String fileName, String filePath) {
        Hub hub = Hub.getInstance();
        RoomView roomView = new RoomView();
        HubController hubController = HubController.getInstance(hub, roomView);
        if (fileName == null) {
            return switch (givenInput[0]) {
                case "open" -> new OpenCommand(givenInput[1], hubController);
                case "help" -> new HelpCommand();
                default -> null;
            };
        } else {
            return switch (givenInput[0]) {
                case "open" -> new OpenCommand(givenInput[1], hubController);
                case "help" -> new HelpCommand();
                case "showdata" -> new TestAllCommand();
                case "close" -> new CloseCommand(hubController, fileName);
                case "saveas" -> new SaveAsCommand(hubController, givenInput[1]);
                case "save" -> new SaveCommand(hubController, filePath);
                case "checkin" ->
                        new CheckInCommand(hubController, givenInput[1], givenInput[2], givenInput[3], givenInput[4], givenInput[5]);
                case "checkout" -> new CheckOutCommand(hubController, givenInput[1]);
                case "availability" -> new AvailabilityCommand(hubController, givenInput[1]);
                case "report" -> new ReportCommand(hubController, givenInput[1], givenInput[2]);
                case "find" -> new FindCommand(hubController, givenInput[1], givenInput[2], givenInput[3]);
                case "unavailable" ->
                        new UnavailableCommand(hubController, givenInput[1], givenInput[2], givenInput[3], givenInput[4]);
                case "forcefind" -> new ForceFindCommand(hubController, givenInput[1], givenInput[2], givenInput[3]);
                default -> null;
            };
        }
    }
}
