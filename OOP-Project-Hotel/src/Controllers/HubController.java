package Controllers;

import Models.Hub;
import Models.Room;
import Views.RoomView;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

/**
 * The {@code HubController} class acts as the controller in the MVC pattern for managing {@link Hub} and {@link Room}.
 * It is responsible for handling the interactions between the {@link Hub} model and the {@link RoomView} view.
 * The class follows a Singleton pattern, allowing only one instance to be created and used throughout the application.
 */
public class HubController {
    private Hub model;
    private RoomView view;
    private static HubController instance;

    /**
     * Private constructor to ensure the Singleton pattern.
     *
     * @param model the {@link Hub} model
     * @param view  the {@link RoomView} view
     */
    private HubController(Hub model, RoomView view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Returns the singleton instance of the {@code HubController}.
     *
     * <p>If the instance has not been created yet, it will create a new one using the provided model and view.
     *
     * @param model the {@link Hub} model
     * @param view  the {@link RoomView} view
     * @return the singleton instance of the {@code HubController}
     */
    public static HubController getInstance(Hub model, RoomView view) {
        if (instance == null) {
            instance = new HubController(model, view);
        }
        return instance;
    }

    /**
     * Displays a list of all rooms in the {@link Hub} by retrieving them from the model and sending them to the view.
     */
    public void listAllRooms() {
        ArrayList<Room> all_rooms = model.returnRooms();
        view.displayRooms(all_rooms);
    }

    /**
     * Adds a {@link Room} to the {@link Hub}.
     *
     * @param room the {@link Room} object to add to the hub
     */
    public void addRoomToHub(Room room) {
        model.addRoom(room);
        System.out.println("Room with number: " + room.getNumber() + " added!");
    }

    /**
     * Retrieves the number of rooms in the {@link Hub}'s room list.
     *
     * @return the number of rooms in the hub
     */
    public int retrieveHubArrayListLength() {
        return model.returnRooms().size();
    }

    /**
     * Clears all the room data from the {@link Hub}.
     */
    public void emptyDataFromHub() {
        model.clearRooms();
        System.out.println("Data cleared!");
    }

    /**
     * Sets a new list of rooms in the {@link Hub}.
     *
     * @param roomsList the new list of {@link Room} objects to set in the hub
     */
    public void setRooms(ArrayList<Room> roomsList) {
        this.model.setRooms(roomsList);
    }

    /**
     * Retrieves the list of rooms from the {@link Hub}.
     *
     * <p>This method is marked with {@link XmlElement} to be used during XML marshalling and unmarshalling.
     *
     * @return the list of rooms in the hub
     */
    @XmlElement(name = "room")
    public ArrayList<Room> retrieveRoomsList() {
        return this.model.getRoomsList();
    }

    /**
     * Searches for and retrieves a {@link Room} by its room number.
     *
     * @param number the room number to search for
     * @return the {@link Room} object with the specified number, or {@code null} if not found
     */
    public Room retrieveRoomByNumber(int number) {
        return this.model.searchRoom(number);
    }

}
