package Core;

import Controllers.CommandFactoryController;
import Core.Utils.CommandNames;
import Core.Utils.OpenedFileOperations;
import Core.Utils.Validators.InputValidator;
import Models.CommandModels.CommandInterface;
import Models.CommandModels.FileCommands.CloseCommand;
import Models.CommandModels.FileCommands.OpenCommand;

import java.util.Scanner;

/**
 * The {@code Engine} class is responsible for running the main logic of the application.
 * It listens for user input, parses commands, validates them, and then executes the respective
 * operations based on the provided command. The engine continuously runs until the "exit" command
 * is received.
 *
 * <p>This class interacts with various utility classes and command models, such as {@link CommandFactoryController},
 * {@link CommandNames}, {@link InputValidator}, and command implementations like {@link OpenCommand} and {@link CloseCommand}.
 */
public class Engine {

    /**
     * Starts and runs the main engine loop.
     *
     * <p>The engine repeatedly reads user input, validates it, and processes commands.
     * It handles file opening, closing, and other operations by delegating tasks to the appropriate command implementations.
     * If a file is currently open, the engine keeps track of the open file's name and path. It ensures that no other
     * operations are performed unless a file is loaded.
     *
     * <p>Special command behaviors:
     * <ul>
     *     <li>{@code availability}: Can be executed without arguments.</li>
     *     <li>{@code exit}: Exits the application.</li>
     * </ul>
     */
    public static void engineRun() {
        CommandFactoryController factory = new CommandFactoryController();
        String openFileName = null;
        String openFilePath = null;

        while (true) {
            String[] instructions = InputReader.parseInput();

            if (instructions[0].equals(String.valueOf((CommandNames.exit)))) {
                System.out.println("Exiting the program...");
                break;
            }
            if (!InputValidator.validateInputLengthAndCommand(instructions)) {
                continue;
            }
            if (instructions[0].equals(String.valueOf(CommandNames.availability)) && instructions.length == 1) {
                instructions = new String[]{"availability", null};
            }

            CommandInterface command = factory.parseCommand(instructions, openFileName, openFilePath);


            if (command instanceof OpenCommand) {
                openFileName = OpenedFileOperations.retrieveFileName(instructions[1]);
                openFilePath = instructions[1];
            } else if (command instanceof CloseCommand) {
                openFileName = null;
            } else if (command == null && openFileName == null) {
                System.out.println("In order to execute other operations please load a file!");
                System.out.println("For help, please use the 'help' command!");
                continue;
            }

            command.executeCommand();

        }
    }
}
