package Core;

import Core.Utils.CommandNames;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * The {@code InputReader} class is responsible for parsing user input commands. It processes the input from the
 * command line, splitting it into individual components and handling specific command formats.
 */
public class InputReader {

    /**
     * Parses user input from the command line into an array of strings, representing the command and its arguments.
     *
     * <p>This method reads a line of input from the user and splits it into command components based on whitespace.
     * It handles special cases for commands like "checkin" and "unavailable" which have specific argument formats.
     *
     * <p>For the "checkin" command, the method distinguishes between the presence of a guest count and a note.
     * It extracts and formats these components accordingly. For the "unavailable" command, it consolidates any
     * additional notes provided by the user.
     *
     * @return an array of strings where the first element is the command and the subsequent elements are the arguments
     */
    public static String[] parseInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter command: ");

        String input = scanner.nextLine();
        String[] parsedCommand = input.split("\\s+");
        parsedCommand[0] = parsedCommand[0].toLowerCase();

        if (parsedCommand[0].equals(String.valueOf(CommandNames.checkin))) {
            String[] parts = input.split(" ");
            String command = parts[0];
            String room = parts[1];
            String from = parts[2];
            String to = parts[3];

            boolean flagHasGuests = false;
            String guests = null;

            try {
                int num = Integer.parseInt(String.valueOf(parts[parts.length - 1]));
                flagHasGuests = true;
            } catch (NumberFormatException e) {
                flagHasGuests = false;
            }
            String note = "";
            if (flagHasGuests == false) {
                for (int i = 4; i < parts.length; i++) {
                    note += parts[i] + " ";
                }
            } else {
                for (int i = 4; i < parts.length - 1; i++) {
                    note += parts[i] + " ";
                }
                guests = parts[parts.length - 1];
            }


            return new String[]{command, room, from, to, note, guests};
        } else if (parsedCommand[0].equals(String.valueOf(CommandNames.unavailable))) {
            String[] parts = input.split(" ");

            String command = parts[0];
            String room = parts[1];
            String fromDate = parts[2];
            String toDate = parts[3];

            StringBuilder noteBuilder = new StringBuilder();
            for (int i = 4; i < parts.length; i++) {
                if (i > 4) {
                    noteBuilder.append(" ");
                }
                noteBuilder.append(parts[i]);
            }
            String note = noteBuilder.toString();

            return new String[]{command, room, fromDate, toDate, note};
        }
        return parsedCommand;
    }


}
