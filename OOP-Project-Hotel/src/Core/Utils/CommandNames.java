package Core.Utils;

/**
 * The {@code CommandNames} enum defines a set of named constants representing the various commands
 * that can be issued in the application. These commands cover file operations, room management, and other actions.
 */
public enum CommandNames {
    open,
    close,
    save,
    saveas,
    help,
    exit,
    checkin,
    availability,
    checkout,
    report,
    find,
    forcefind,
    unavailable,
    showdata;

}
