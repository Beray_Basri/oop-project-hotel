package Core.Utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The {@code LocalDateAdapter} class is a JAXB adapter that facilitates the conversion between {@link LocalDate}
 * objects and their XML representation as strings. It uses a specific date format for serialization and deserialization.
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    /**
     * Converts a string value from XML to a {@link LocalDate} object.
     *
     * <p>This method parses the string representation of the date using the format "dd-MM-yyyy" and returns the
     * corresponding {@code LocalDate} object.
     *
     * @param v the string value from XML to be converted
     * @return the {@code LocalDate} object represented by the string
     * @throws Exception if parsing the date fails
     */
    @Override
    public LocalDate unmarshal(String v) throws Exception {
        return LocalDate.parse(v, formatter);
    }

    /**
     * Converts a {@link LocalDate} object to its string representation for XML serialization.
     *
     * <p>This method formats the {@code LocalDate} object as a string using the format "dd-MM-yyyy".
     *
     * @param v the {@code LocalDate} object to be converted
     * @return the string representation of the date
     * @throws Exception if formatting the date fails
     */
    @Override
    public String marshal(LocalDate v) throws Exception {
        return v.format(formatter);
    }
}
