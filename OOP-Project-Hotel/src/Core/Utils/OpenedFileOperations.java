package Core.Utils;

import java.io.File;

/**
 * The {@code OpenedFileOperations} class provides utility methods for operations related to files.
 * Specifically, it includes methods for retrieving file information from a given file path.
 */
public class OpenedFileOperations {

    /**
     * Retrieves the name of the file from a given file path.
     *
     * <p>This method extracts the file name from the full file path. It uses the {@link File#getName()} method
     * to obtain the name of the file.
     *
     * @param filePath the full path to the file
     * @return the name of the file
     */
    public static String retrieveFileName(String filePath){
        File file = new File(filePath);
        return file.getName();
    }


}
