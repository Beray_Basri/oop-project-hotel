package Core.Utils.Validators;

import Controllers.HubController;
import Models.Hub;
import Models.Room;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * The {@code FileValidators} class provides utility methods to validate files related to the {@link Hub} model.
 * These methods include checks for whether a file is a valid XML file, whether a file is open, and whether
 * a file exists in a given directory.
 */
public class FileValidators {

    /**
     * Checks if the file at the given path is a valid XML file that can be unmarshalled into a {@link Hub} object.
     *
     * <p>This method attempts to create a JAXB context for the {@link Hub} class and unmarshals the file to verify
     * that it is a valid XML file. If the file cannot be read or is not a valid XML, the method returns {@code false}.
     *
     * @param filePath the path to the file to check
     * @return {@code true} if the file is a valid XML file, {@code false} otherwise
     */
    public static boolean checkFileIsXML(String filePath) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Hub.class);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            try (InputStream inputStream = new FileInputStream(filePath)) {
                unmarshaller.unmarshal(inputStream);
                return true;
            }
        } catch (IOException | JAXBException e) {
            return false;
        }
    }

    /**
     * Checks if there is an open file in the given {@link HubController}.
     *
     * <p>This method checks if the hub managed by the controller contains any rooms. If it does, it indicates that
     * a file has been opened, and the method returns {@code true}.
     *
     * @param hubController the {@link HubController} managing the hub
     * @return {@code true} if a file is open, {@code false} otherwise
     */
    public static boolean fileIsOpen(HubController hubController) {
        return hubController.retrieveHubArrayListLength() != 0;
    }

    /**
     * Checks whether a file with the specified name exists in the given directory.
     *
     * <p>This method looks for the file in the directory and returns {@code true} if the file exists and is a regular file.
     *
     * @param directoryPath the path to the directory to check
     * @param fileName      the name of the file to search for in the directory
     * @return {@code true} if the file exists in the directory, {@code false} otherwise
     */
    public static boolean fileExistsInDirectory(String directoryPath, String fileName) {
        File directory = new File(directoryPath);
        File fileToCheck = new File(directory, fileName);

        //file exists in directory -> return true else false
        return fileToCheck.exists() && fileToCheck.isFile();
    }

}
