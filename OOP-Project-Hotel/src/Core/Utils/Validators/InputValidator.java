package Core.Utils.Validators;

import Core.Utils.CommandNames;
import Models.CommandModels.CommandInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * The {@code InputValidator} class is responsible for validating user input based on command names and their expected
 * argument lengths. It ensures that the given input matches the expected command format before being processed.
 */
public class InputValidator {

    /**
     * Validates the length of the given input and checks if the command is valid.
     *
     * <p>This method checks the command name and verifies that the number of parameters supplied matches the
     * expected number for that command. It uses a predefined set of commands and their expected parameter counts.
     *
     * <p>Supported commands include both file operations (like "open", "save", "close") and hub operations
     * (like "checkin", "checkout", "availability").
     *
     * @param instructions an array of strings representing the command and its parameters
     * @return {@code true} if the input length matches the expected command format, {@code false} otherwise
     */
    public static boolean validateInputLengthAndCommand(String[] instructions) {
        if (instructions[0].equals(String.valueOf(CommandNames.checkin)) || instructions[0].equals(String.valueOf(CommandNames.checkout)) || instructions[0].equals(String.valueOf(CommandNames.availability))) {
            return true;
        }

        Map<String, Integer> dictionary = new HashMap<>();
        //File operations
        dictionary.put(String.valueOf(CommandNames.open), 2);
        dictionary.put(String.valueOf(CommandNames.close), 1);
        dictionary.put(String.valueOf(CommandNames.save), 1);
        dictionary.put(String.valueOf(CommandNames.saveas), 2);
        dictionary.put(String.valueOf(CommandNames.help), 1);
        dictionary.put(String.valueOf(CommandNames.exit), 1);

        //Hub operations
        dictionary.put(String.valueOf(CommandNames.checkin), 6);
        dictionary.put(String.valueOf(CommandNames.availability), 2);
        dictionary.put(String.valueOf(CommandNames.checkout), 1);
        dictionary.put(String.valueOf(CommandNames.report), 3);
        dictionary.put(String.valueOf(CommandNames.find), 4);
        dictionary.put(String.valueOf(CommandNames.forcefind), 4);
        dictionary.put(String.valueOf(CommandNames.unavailable), 5);
        dictionary.put(String.valueOf(CommandNames.showdata), 1);


        if (dictionary.get(instructions[0]) == null) {
            System.out.println("Command " + instructions[0] + " invalid!");
            return false;
        }
        if (instructions.length != dictionary.get(instructions[0])) {
            System.out.println("Invalid command parameters!");
            return false;
        }
        return true;


    }

}
