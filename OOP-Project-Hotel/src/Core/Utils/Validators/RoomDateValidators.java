package Core.Utils.Validators;

import java.time.LocalDate;
import java.time.LocalTime;


/**
 * The {@code RoomDateValidators} class provides utility methods to validate date ranges related to room bookings.
 * It checks whether a room's accommodation period overlaps with another date range or whether the room is available
 * on a specific date.
 */
public class RoomDateValidators {

    /**
     * Validates that the "from" date is not after the "to" date.
     *
     * <p>This method ensures that a given date range is valid by checking that the start date is not later
     * than the end date.
     *
     * @param from the start date
     * @param to   the end date
     * @return {@code true} if the date range is valid, {@code false} otherwise
     */
    public static boolean validateDate(LocalDate from, LocalDate to) {
        return !to.isBefore(from) && !from.isAfter(to);
    }

    /**
     * Checks if the room's accommodation period overlaps or equals the specified date range.
     *
     * <p>This method compares the room's booking period (from {@code roomFrom} to {@code roomTo}) with another
     * specified period (from {@code from} to {@code to}). It returns {@code true} if the room's period overlaps
     * with or is equal to the specified period.
     *
     * @param roomFrom the start date of the room's booking
     * @param roomTo   the end date of the room's booking
     * @param from     the start date of the period to compare
     * @param to       the end date of the period to compare
     * @return {@code true} if the room's booking period overlaps with the specified period, {@code false} otherwise
     */
    public static boolean checkRoomDates(LocalDate roomFrom, LocalDate roomTo, LocalDate from, LocalDate to) {
        boolean flagFrom = false;
        boolean flagTo = false;

        if (roomFrom.isAfter(from) || roomFrom.isEqual(from)) {
            flagFrom = true;
        }
        if (roomTo.isBefore(to) || roomTo.isEqual(to)) {
            flagTo = true;
        }
        return flagFrom && flagTo;
    }

    /**
     * Checks if the room is available (empty) on a specific date.
     *
     * <p>This method checks whether the specified {@code date} is before the room's check-in date ({@code roomFrom})
     * or after its check-out date ({@code roomTo}). If the date is outside the booking period, the room is considered
     * available, and the method returns {@code true}.
     *
     * @param date     the date to check for room availability
     * @param roomFrom the room's check-in date
     * @param roomTo   the room's check-out date
     * @return {@code true} if the room is available on the specified date, {@code false} otherwise
     */
    public static boolean checkRoomEmptyOnGivenDate(LocalDate date, LocalDate roomFrom, LocalDate roomTo) {
        return date.isBefore(roomFrom) || date.isAfter(roomTo);
    }
}
