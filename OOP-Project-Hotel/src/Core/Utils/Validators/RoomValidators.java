package Core.Utils.Validators;

import Models.Room;

import java.time.LocalDate;
import java.util.Locale;

/**
 * The {@code RoomValidators} class provides utility methods to validate various aspects of room availability,
 * status, and capacity. It helps check if a room is available, empty, and if it has enough beds to meet the requirements.
 */
public class RoomValidators {

    /**
     * Validates if the given room is available for booking.
     *
     * <p>This method checks the room's availability status and returns {@code true} if the room is available,
     * meaning it is not currently occupied.
     *
     * @param room the {@link Room} object to validate
     * @return {@code true} if the room is available, {@code false} otherwise
     */
    public static boolean validateRoomIsAvailable(Room room) {
        return room.getStatus();
    }

    /**
     * Validates if the room is empty, meaning it has not been checked into.
     *
     * <p>This method checks if the room's check-in date is set to a placeholder value of January 1st, 1000
     * (used to indicate no active bookings). If so, the room is considered empty.
     *
     * @param room the {@link Room} object to validate
     * @return {@code true} if the room is empty, {@code false} otherwise
     */
    public static boolean validateRoomIsEmpty(Room room) {
        LocalDate toCompare = LocalDate.of(1000, 1, 1);
        boolean comparisonResult = room.getCheckIn().equals(toCompare);
        return comparisonResult;
    }

    /**
     * Validates if the room has enough beds to meet the specified number of needed beds.
     *
     * <p>This method checks if the room's bed count is greater than or equal to the number of beds required.
     *
     * @param numberOfNeededBeds the number of beds required
     * @param room               the {@link Room} object to validate
     * @return {@code true} if the room has enough beds, {@code false} otherwise
     */
    public static boolean validateEnoughBedsInRoom(int numberOfNeededBeds, Room room) {
        return room.getBeds() >= numberOfNeededBeds;
    }

}
