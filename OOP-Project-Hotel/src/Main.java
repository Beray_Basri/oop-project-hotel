import Core.Engine;

/**
 * The {@code Main} class serves as the entry point for the application.
 * <p>
 * It contains the {@code main} method, which initiates the execution
 * of the application by invoking the {@link Engine#engineRun()} method.
 */
public class Main {

    /**
     * The main method is the entry point for the program.
     * It starts the application by calling the {@link Engine#engineRun()} method
     * to execute the core logic of the system.
     *
     * @param args command-line arguments passed during the program execution (if any).
     */
    public static void main(String[] args) {
        Engine.engineRun();
    }
}