package Models.CommandModels;

/**
 * Represents a command that can be executed.
 * <p>
 * This interface defines a single method, {@link #executeCommand()}, which is implemented by
 * classes that represent specific commands. The purpose of the command pattern is to encapsulate
 * a request as an object, thereby allowing for parameterization of clients with different requests,
 * queuing of requests, and logging of the requests.
 * </p>
 * <p>
 * Implementing classes are expected to provide the logic for executing the command when the
 * {@code executeCommand} method is invoked.
 * </p>
 */
public interface CommandInterface {

    /**
     * Executes the command.
     * <p>
     * This method is invoked to perform the action encapsulated by the command. The exact behavior
     * of this method depends on the specific implementation of the command.
     * </p>
     */
    void executeCommand();
}
