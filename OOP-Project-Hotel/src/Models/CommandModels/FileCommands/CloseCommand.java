package Models.CommandModels.FileCommands;

import Controllers.HubController;
import Core.Utils.Validators.FileValidators;
import Models.CommandModels.CommandInterface;

/**
 * The {@code CloseCommand} class implements the {@link CommandInterface} and defines the behavior for closing
 * an open file. It clears data from the {@link HubController} and provides feedback to the user.
 */
public class CloseCommand implements CommandInterface {
    private HubController hubController;
    private String fileName;

    /**
     * Constructs a {@code CloseCommand} with the specified {@link HubController} and file name.
     *
     * <p>This constructor initializes the command with the necessary components to execute the close operation.
     *
     * @param hubController the {@link HubController} instance to interact with
     * @param fileName      the name of the file to be closed
     */
    public CloseCommand(HubController hubController, String fileName) {
        this.hubController = hubController;
        this.fileName = fileName;
    }

    /**
     * Executes the close command by checking if a file is currently open and then clearing data from the {@link HubController}.
     *
     * <p>If no file is open, it prints a message indicating that the file must be opened first. If a file is open, it
     * clears the data from the {@code HubController} and confirms the successful closure of the file.
     */
    @Override
    public void executeCommand() {
        if (!FileValidators.fileIsOpen(this.hubController)) {
            System.out.println("File not opened! Please open a file first!");
            return;
        }
        hubController.emptyDataFromHub();
        System.out.println("Successfully closed " + fileName);
    }
}
