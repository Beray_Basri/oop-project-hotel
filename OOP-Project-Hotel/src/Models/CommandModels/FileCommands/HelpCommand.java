package Models.CommandModels.FileCommands;

import Models.CommandModels.CommandInterface;
import Views.HelpCommandView;

/**
 * The {@code HelpCommand} class implements the {@link CommandInterface} and provides the functionality to display
 * help information to the user. This command is used to assist users by showing available commands and their usage.
 */
public class HelpCommand implements CommandInterface {

    /**
     * Executes the help command by invoking the {@link HelpCommandView#displayHelp()} method.
     *
     * <p>This method provides the user with information about available commands and their usage through the
     * {@code HelpCommandView} class.
     */
    @Override
    public void executeCommand() {
        HelpCommandView.displayHelp();
    }
}
