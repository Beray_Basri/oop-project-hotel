package Models.CommandModels.FileCommands;

import Controllers.CommandFactoryController;
import Controllers.HubController;
import Core.Utils.OpenedFileOperations;
import Core.Utils.Validators.FileValidators;
import Models.CommandModels.CommandInterface;
import Models.Hub;
import Models.Room;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The {@code OpenCommand} class implements the {@link CommandInterface} and defines the behavior for opening
 * and loading data from an XML file into the system. It handles file validation, parsing, and data loading.
 */
public class OpenCommand implements CommandInterface {
    private String filePath;
    private HubController hubController;

    /**
     * Constructs an {@code OpenCommand} with the specified file path and {@link HubController}.
     *
     * <p>This constructor initializes the command with the file path to open and the {@code HubController} to manage
     * the data.
     *
     * @param filePath      the path to the XML file to be opened
     * @param hubController the {@link HubController} instance to interact with
     */
    public OpenCommand(String filePath, HubController hubController) {
        this.filePath = filePath;
        this.hubController = hubController;
    }

    /**
     * Executes the open command by performing the following steps:
     * <ol>
     *   <li>Validates that the file is in XML format using {@link FileValidators#checkFileIsXML(String)}.</li>
     *   <li>Checks if a file is already opened using {@link FileValidators#fileIsOpen(HubController)}.</li>
     *   <li>If the file is valid and no file is currently open, parses the XML file using JAXB to load data.</li>
     *   <li>Adds the loaded {@link Room} objects to the {@code HubController}.</li>
     *   <li>Prints a success message indicating the file has been successfully opened.</li>
     * </ol>
     */
    @Override
    public void executeCommand() {
        File xmlFile = new File(filePath);

        if (!xmlFile.exists()) {
            System.out.println("File not found! Creating a new empty file: " + filePath);
            createEmptyHubFile(xmlFile);
            return;
        }

        if (!FileValidators.checkFileIsXML(filePath)) {
            System.out.println("Error while loading file! Please provide a valid XML file that meets the requirements of the application!");
            return;
        }
        if (FileValidators.fileIsOpen(this.hubController)) {
            System.out.println("File is already opened! Please close the previous file first!");
            return;
        }

        ArrayList<Room> roomsList;
        try {
            JAXBContext context = JAXBContext.newInstance(Hub.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            Hub hub = (Hub) unmarshaller.unmarshal(xmlFile);

            roomsList = hub.getRoomsList();

        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }

        for (Room room : roomsList) {
            hubController.addRoomToHub(room);
        }
        System.out.println("Successfully opened " + OpenedFileOperations.retrieveFileName(this.filePath));

    }

    /**
     * Creates an empty Hub XML file if the specified file doesn't exist.
     *
     * @param xmlFile The file to be created.
     */
    private void createEmptyHubFile(File xmlFile) {
        try {
            if (xmlFile.createNewFile()) {
                Hub emptyHub = new Hub();
                emptyHub.setRooms(new ArrayList<>());

                JAXBContext context = JAXBContext.newInstance(Hub.class);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

                marshaller.marshal(emptyHub, xmlFile);

                System.out.println("Empty XML file created successfully: " + xmlFile.getName());
            }
        } catch (IOException | JAXBException e) {
            System.out.println("Error while creating the file: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
