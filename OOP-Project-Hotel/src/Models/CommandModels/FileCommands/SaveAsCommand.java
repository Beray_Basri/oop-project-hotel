package Models.CommandModels.FileCommands;

import Controllers.HubController;
import Core.Utils.Validators.FileValidators;
import Models.CommandModels.CommandInterface;
import Models.Hub;
import Models.Room;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.File;
import java.util.ArrayList;

/**
 * The {@code SaveAsCommand} class implements the {@link CommandInterface} and defines the behavior for saving
 * the current state of the {@link Hub} to a specified XML file. It allows saving data to a new file location
 * or name.
 */
public class SaveAsCommand implements CommandInterface {
    private HubController hubController;
    private String filePath;

    /**
     * Constructs a {@code SaveAsCommand} with the specified {@link HubController} and file path.
     *
     * <p>This constructor initializes the command with the path where the XML file will be saved and the
     * {@code HubController} to retrieve the data to be saved.
     *
     * @param hubController the {@link HubController} instance to interact with and get data from
     * @param filePath      the path to the XML file where the data will be saved
     */
    public SaveAsCommand(HubController hubController, String filePath) {
        this.hubController = hubController;
        this.filePath = filePath;
    }

    /**
     * Executes the save command by performing the following steps:
     * <ol>
     *   <li>Checks if a file with the specified name already exists in the directory using
     *       {@link FileValidators#fileExistsInDirectory(String, String)}.</li>
     *   <li>If the file exists, prints an error message and aborts the operation.</li>
     *   <li>If the file does not exist, creates a new XML file using JAXB and saves the current state of the
     *       {@code Hub} (retrieved from the {@code HubController}) to this file.</li>
     *   <li>Prints a success message indicating that the data has been successfully saved.</li>
     * </ol>
     *
     * @throws RuntimeException if an error occurs during JAXB operations
     */
    @Override
    public void executeCommand() {
        try {
            File file = new File(filePath);

            if (FileValidators.fileExistsInDirectory(this.filePath, file.getName())) {
                System.out.println("File with given name already exists in the directory!");
                return;
            }

            JAXBContext context = JAXBContext.newInstance(Hub.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            Hub wrapper = new Hub();
            wrapper.setRooms(hubController.retrieveRoomsList());

            marshaller.marshal(wrapper, file);

            System.out.println("Successfully saved " + file.getName());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}

