package Models.CommandModels.FileCommands;

import Controllers.HubController;
import Models.CommandModels.CommandInterface;
import Models.Hub;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

/**
 * The {@code SaveCommand} class implements the {@link CommandInterface} and defines the behavior for saving
 * the current state of the {@link Hub} to the XML file that is currently opened. It updates the existing file
 * with the latest data.
 */
public class SaveCommand implements CommandInterface {
    private HubController hubController;
    private String openFilePath;

    /**
     * Constructs a {@code SaveCommand} with the specified {@link HubController} and the path of the currently
     * opened file.
     *
     * <p>This constructor initializes the command with the path of the XML file where the data will be saved and
     * the {@code HubController} to retrieve the data to be saved.
     *
     * @param hubController the {@link HubController} instance to interact with and get data from
     * @param openFilePath the path of the XML file where the data will be saved
     */
    public SaveCommand(HubController hubController, String openFilePath) {
        this.hubController = hubController;
        this.openFilePath = openFilePath;
    }

    /**
     * Executes the save command by performing the following steps:
     * <ol>
     *   <li>Creates a {@link File} object from the specified file path.</li>
     *   <li>Uses JAXB to marshal the current state of the {@code Hub} (retrieved from the {@code HubController})
     *       into the specified XML file.</li>
     *   <li>Prints a success message indicating that the data has been successfully saved.</li>
     * </ol>
     *
     * @throws RuntimeException if an error occurs during JAXB operations
     */
    @Override
    public void executeCommand() {
        try {
            File file = new File(this.openFilePath);

            JAXBContext context = JAXBContext.newInstance(Hub.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            Hub wrapper = new Hub();
            wrapper.setRooms(hubController.retrieveRoomsList());

            marshaller.marshal(wrapper, file);

            System.out.println("Successfully saved " + file.getName());
        } catch (JAXBException ignored) {
        }
    }

}
