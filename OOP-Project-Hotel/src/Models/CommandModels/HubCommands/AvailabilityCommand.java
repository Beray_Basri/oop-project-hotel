package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Models.CommandModels.CommandInterface;
import Views.AvailabilityCommandView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The {@code AvailabilityCommand} class implements the {@link CommandInterface} and defines the behavior
 * for checking and displaying available rooms on a specified date.
 */
public class AvailabilityCommand implements CommandInterface {
    private HubController hubController;
    private String date;

    /**
     * Constructs an {@code AvailabilityCommand} with the specified {@link HubController} and date.
     *
     * <p>This constructor initializes the command with the {@code HubController} to interact with and the date
     * for which room availability needs to be checked.
     *
     * @param hubController the {@link HubController} instance to interact with and get room data from
     * @param date          the date for which to check room availability, formatted as "dd-MM-yyyy"
     */
    public AvailabilityCommand(HubController hubController, String date) {
        this.hubController = hubController;
        this.date = date;
    }

    /**
     * Executes the command by performing the following steps:
     * <ol>
     *   <li>Parses the specified date string into a {@link LocalDate} object using the "dd-MM-yyyy" format.
     *       If parsing fails, it defaults to the current date.</li>
     *   <li>Prints a message indicating the date for which the room availability is being checked.</li>
     *   <li>Uses {@link AvailabilityCommandView} to display available rooms for the specified date.</li>
     * </ol>
     *
     * @see AvailabilityCommandView#showAvailableRooms(HubController, LocalDate)
     */
    @Override
    public void executeCommand() {
        AvailabilityCommandView commandView = new AvailabilityCommandView();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = null;
        try {
            date = LocalDate.parse(this.date, formatter);
        } catch (Exception e) {
            date = LocalDate.now();
        }

        System.out.println("Empty rooms on " + date);
        commandView.showAvailableRooms(hubController, date);


    }
}
