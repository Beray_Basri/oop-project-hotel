package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Core.Utils.Validators.RoomValidators;
import Models.CommandModels.CommandInterface;
import Models.Room;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The {@code CheckInCommand} class implements the {@link CommandInterface} and provides the functionality
 * to check a guest into a room for a specified date range.
 */
public class CheckInCommand implements CommandInterface {
    private HubController hubController;
    private String room;
    private String checkIn;
    private String checkOut;
    private String note;
    private String guests;

    /**
     * Constructs a {@code CheckInCommand} with the specified parameters.
     *
     * <p>This constructor initializes the command with the {@link HubController} to interact with and
     * details for checking in a guest, including room number, check-in and check-out dates, notes, and number of guests.
     *
     * @param hubController the {@link HubController} instance to interact with and get room data from
     * @param room          the room number where the guest will be checked in
     * @param checkIn       the check-in date formatted as "dd-MM-yyyy"
     * @param checkOut      the check-out date formatted as "dd-MM-yyyy"
     * @param note          additional notes for the room
     * @param guests        the number of guests; if {@code null}, the room will be set to its full capacity
     */
    public CheckInCommand(HubController hubController, String room, String checkIn, String checkOut, String note, String guests) {
        this.hubController = hubController;
        this.room = room;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.note = note;
        this.guests = guests;
    }

    /**
     * Executes the command by performing the following steps:
     * <ol>
     *   <li>Retrieves the room with the specified room number from the {@link HubController}.</li>
     *   <li>Parses the check-in and check-out dates from the provided strings.</li>
     *   <li>Validates if the room exists, is available, and is empty.</li>
     *   <li>Checks if the number of guests fits the room's capacity.</li>
     *   <li>Validates that the check-in date is not after the check-out date and vice versa.</li>
     *   <li>Updates the room's check-in and check-out dates, note, and number of guests.</li>
     *   <li>Prints the details of the checked-in room.</li>
     * </ol>
     */
    @Override
    public void executeCommand() {
        Room room = hubController.retrieveRoomByNumber(Integer.parseInt(this.room));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate parsedCheckIn = LocalDate.parse(this.checkIn, formatter);
        LocalDate parsedCheckOut = LocalDate.parse(this.checkOut, formatter);

        if (hubController.retrieveRoomByNumber(Integer.parseInt(this.room)) == null) {
            System.out.println("Room " + this.room + " not found!");
            return;
        }
        if (!RoomValidators.validateRoomIsAvailable(room)) {
            System.out.println("Room " + this.room + " is unavailable!");
            return;
        }
        if (!RoomValidators.validateRoomIsEmpty(room)) {
            System.out.println("Room " + this.room + " is not empty!");
            return;
        }
        if (this.guests != null && !RoomValidators.validateEnoughBedsInRoom(Integer.parseInt(this.guests), room)) {
            System.out.println("Room " + this.room + " doesn't have enough beds!");
            return;
        }
        if (!RoomDateValidators.validateDate(parsedCheckIn, parsedCheckOut)) {
            System.out.println("Check in date cannot be after check out date and vice versa!");
            return;
        }

        room.setCheckIn(parsedCheckIn);
        room.setCheckOut(parsedCheckOut);
        room.setNote(this.note);
        if (this.guests == null) {
            room.setGuests(room.getBeds());
        } else {
            room.setGuests(Integer.parseInt(this.guests));
        }

        System.out.print("Checked in room -> ");
        System.out.println(room);
    }
}



