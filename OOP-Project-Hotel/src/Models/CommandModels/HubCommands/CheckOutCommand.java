package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Core.Utils.Validators.RoomValidators;
import Models.CommandModels.CommandInterface;
import Models.Room;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The {@code CheckOutCommand} class implements the {@link CommandInterface} and provides the functionality
 * to check a guest out of a room, making it available for new bookings.
 */
public class CheckOutCommand implements CommandInterface {
    private HubController hubController;
    private String roomNumber;

    /**
     * Constructs a {@code CheckOutCommand} with the specified parameters.
     *
     * <p>This constructor initializes the command with the {@link HubController} to interact with and
     * the room number for which the guest will be checked out.
     *
     * @param hubController the {@link HubController} instance to interact with and get room data from
     * @param room          the room number from which the guest will be checked out
     */
    public CheckOutCommand(HubController hubController, String room) {
        this.hubController = hubController;
        this.roomNumber = room;
    }

    /**
     * Executes the command by performing the following steps:
     * <ol>
     *   <li>Retrieves the room with the specified room number from the {@link HubController}.</li>
     *   <li>Validates if the room exists and if it is not already empty.</li>
     *   <li>Resets the room's check-in and check-out dates to a default empty date.</li>
     *   <li>Sets the number of guests to zero and clears any notes associated with the room.</li>
     *   <li>Updates the room status to available if it is not already.</li>
     *   <li>Prints a confirmation message indicating the room has been checked out.</li>
     * </ol>
     */
    @Override
    public void executeCommand() {
        Room room = hubController.retrieveRoomByNumber(Integer.parseInt(this.roomNumber));

        if (room == null) {
            System.out.println("Room " + this.roomNumber + " not found!");
            return;
        }
        if (RoomValidators.validateRoomIsEmpty(room)) {
            System.out.println("Room " + this.roomNumber + " already empty!");
            return;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate emptyDate = LocalDate.parse("01-01-1000", formatter);

        room.setGuests(0);
        room.setCheckIn(emptyDate);
        room.setCheckOut(emptyDate);
        room.setNote(null);
        if (!room.getStatus()) {
            room.setStatus(true);
        }

        System.out.println("Checked out of room " + this.roomNumber + "!");
    }
}
