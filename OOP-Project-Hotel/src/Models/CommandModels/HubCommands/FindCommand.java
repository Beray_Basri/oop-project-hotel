package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Models.CommandModels.CommandInterface;
import Views.FindCommandView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The {@code FindCommand} class implements the {@link CommandInterface} and provides the functionality
 * to find available rooms based on specified criteria including the number of beds and a date range.
 */
public class FindCommand implements CommandInterface {
    private HubController hubController;
    private String beds;
    private String fromDate;
    private String toDate;

    /**
     * Constructs a {@code FindCommand} with the specified parameters.
     *
     * <p>This constructor initializes the command with the {@link HubController} to interact with and
     * the criteria for finding rooms including the number of beds and the date range.
     *
     * @param hubController the {@link HubController} instance to interact with and get room data from
     * @param fromDate      the start date of the period to search for available rooms
     * @param toDate        the end date of the period to search for available rooms
     * @param beds          the number of beds required in the rooms
     */
    public FindCommand(HubController hubController,String beds, String fromDate, String toDate) {
        this.hubController = hubController;
        this.beds = beds;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    /**
     * Executes the command by performing the following steps:
     * <ol>
     *   <li>Parses the provided dates into {@link LocalDate} objects.</li>
     *   <li>Checks if the parsed dates are valid; if not, uses a default empty date.</li>
     *   <li>Prints a message indicating the search criteria for available rooms.</li>
     *   <li>Delegates the task of finding and displaying the rooms to the {@link FindCommandView} class.</li>
     * </ol>
     */
    @Override
    public void executeCommand() {
        FindCommandView findCommandView = new FindCommandView();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dateFromParsed = null, dateToParsed = null;
        LocalDate emptyDate = LocalDate.parse("01-01-1000", formatter);
        try {
            dateFromParsed = LocalDate.parse(this.fromDate, formatter);
            dateToParsed = LocalDate.parse(this.toDate, formatter);
        } catch (Exception ignored) {
        }

        System.out.println("Empty rooms with " + beds + " beds from " + this.fromDate + " to " + this.toDate);
        findCommandView.showFoundRooms(hubController, dateFromParsed, dateToParsed, Integer.parseInt(this.beds), emptyDate);
    }
}
