package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Models.CommandModels.CommandInterface;
import Models.Room;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * The {@code ForceFindCommand} class implements the {@link CommandInterface} and provides functionality
 * to forcefully find and reallocate guests to rooms based on specified criteria, such as the number of beds
 * and a date range. This command allows for guest accommodation even when specific room constraints exist.
 */
public class ForceFindCommand implements CommandInterface {
    private HubController hubController;
    private String beds;
    private String dateFrom;
    private String dateTo;

    /**
     * Constructs a {@code ForceFindCommand} with the specified parameters.
     *
     * <p>This constructor initializes the command with the {@link HubController} to interact with and
     * the criteria for finding and reallocating rooms, including the number of beds and the date range.
     *
     * @param hubController the {@link HubController} instance to interact with and get room data from
     * @param beds          the number of beds required in the rooms
     * @param dateFrom      the start date of the period to search for available rooms
     * @param dateTo        the end date of the period to search for available rooms
     */
    public ForceFindCommand(HubController hubController, String beds, String dateFrom, String dateTo) {
        this.hubController = hubController;
        this.beds = beds;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    /**
     * Executes the command by performing the following steps:
     * <ol>
     *   <li>Parses the provided dates into {@link LocalDate} objects.</li>
     *   <li>Validates the parsed dates to ensure that the check-in date is before or the same as the check-out date.</li>
     *   <li>Prompts the user to specify which rooms to free and move guests into. Validates the room status and occupancy.</li>
     *   <li>Reallocates guests from one room to another and updates room details accordingly.</li>
     *   <li>Prints messages indicating the success or failure of the command execution.</li>
     * </ol>
     */
    @Override
    public void executeCommand() {
        Scanner scanner = new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dateFromParsed = null, dateToParsed = null;
        try {
            dateFromParsed = LocalDate.parse(this.dateFrom, formatter);
            dateToParsed = LocalDate.parse(this.dateTo, formatter);
        } catch (Exception ignored) {
        }

        if (!RoomDateValidators.validateDate(dateFromParsed, dateToParsed)) {
            System.out.println("Check in date cannot be after check out date and vice versa!");
            return;
        }


        int taken_beds = 0;
        LocalDate toCheck = LocalDate.of(1000, 01, 01);

        for (int i = 0; i < 2; i++) {
            System.out.print(i + 1 + "-> Enter number of room that you want to free: ");
            int given_room_number = scanner.nextInt();
            System.out.print(i + 1 + "-> Enter number of room that you want to move guests: ");
            int free_room_number = scanner.nextInt();

            Room found_room = hubController.retrieveRoomByNumber(given_room_number);
            Room found_free_room = hubController.retrieveRoomByNumber(free_room_number);

            if (found_room == null || found_free_room == null) {
                i--;
                System.out.println("Room not found!");
                continue;
            }
            if (!found_free_room.getStatus()) {
                i--;
                System.out.println("Guests cant be moved into unavailable room!");
                continue;
            }
            if (!found_free_room.getCheckIn().equals(toCheck)) {
                i--;
                System.out.println("Cannot move guests to already occupied room!");
                continue;
            }

            found_free_room.setCheckIn(found_room.getCheckIn());
            found_free_room.setCheckOut(found_room.getCheckOut());
            found_free_room.setNote("Moved room for VIP guest");
            found_free_room.setGuests(found_room.getGuests());

            found_room.setCheckIn(dateFromParsed);
            found_room.setCheckOut(dateToParsed);
            found_room.setNote("VIP guest");
            if (i == 0) {
                found_room.setGuests(found_room.getBeds());
            } else {
                found_room.setGuests(Integer.parseInt(this.beds) - taken_beds);
            }
            taken_beds += found_room.getBeds();

            if (Integer.parseInt(this.beds) == taken_beds) {
                System.out.println("Guests successfully accommodated!");
                break;
            }

        }
        System.out.println("Force find command completed successfully!");

    }
}
