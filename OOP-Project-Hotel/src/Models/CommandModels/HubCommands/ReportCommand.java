package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Models.CommandModels.CommandInterface;
import Views.ReportCommandView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The {@code ReportCommand} class implements the {@link CommandInterface} and provides functionality
 * to generate a report of rooms based on a specified date range. This command retrieves and displays
 * information about rooms available within the given dates.
 */
public class ReportCommand implements CommandInterface {
    private HubController hubController;
    private String dateFrom;
    private String dateTo;

    /**
     * Constructs a {@code ReportCommand} with the specified parameters.
     *
     * <p>This constructor initializes the command with the {@link HubController} to interact with and
     * the date range for generating the report.
     *
     * @param hubController the {@link HubController} instance to interact with and get room data from
     * @param dateFrom      the start date of the period for which to generate the report
     * @param dateTo        the end date of the period for which to generate the report
     */
    public ReportCommand(HubController hubController, String dateFrom, String dateTo) {
        this.hubController = hubController;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    /**
     * Executes the command by performing the following steps:
     * <ol>
     *   <li>Parses the provided dates into {@link LocalDate} objects.</li>
     *   <li>Initializes a {@link ReportCommandView} instance for displaying the report.</li>
     *   <li>Prints a header message indicating that rooms are being found.</li>
     *   <li>Calls the {@link ReportCommandView#showReport(HubController, LocalDate, LocalDate)} method
     *       to display the report of rooms available within the specified date range.</li>
     * </ol>
     */
    @Override
    public void executeCommand() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dateFromParsed = null, dateToParsed = null;
        try {
            dateFromParsed = LocalDate.parse(this.dateFrom, formatter);
            dateToParsed = LocalDate.parse(this.dateTo, formatter);
        } catch (Exception ignored) {
        }

        ReportCommandView reportCommandView = new ReportCommandView();
        reportCommandView.showReport(hubController, dateFromParsed, dateToParsed);
    }
}
