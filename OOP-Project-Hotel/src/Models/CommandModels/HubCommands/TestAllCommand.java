package Models.CommandModels.HubCommands;

import Models.CommandModels.CommandInterface;
import Models.Hub;
import Models.Room;

/**
 * Command implementation for testing and debugging purposes by displaying all room details.
 * <p>
 * This command retrieves the list of rooms from the {@link Hub} singleton instance and prints
 * the details of each room to the console. It is typically used for testing the state of the
 * room data in the system.
 * </p>
 * <p>
 * Steps performed by this command:
 * <ol>
 *   <li>Retrieves the list of rooms from the {@link Hub} instance.</li>
 *   <li>Prints the details of each room in the list.</li>
 * </ol>
 * </p>
 */
public class TestAllCommand implements CommandInterface {
    /**
     * Executes the command to display all room details.
     * <p>
     * Retrieves the singleton instance of the {@link Hub}, obtains the list of {@link Room} objects,
     * and prints the details of each room to the console.
     * </p>
     */
    @Override
    public void executeCommand() {
        Hub hub = Hub.getInstance();
        for (Room room : hub.getRoomsList()) {
            System.out.println(room);
        }
    }
}
