package Models.CommandModels.HubCommands;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Core.Utils.Validators.RoomValidators;
import Models.CommandModels.CommandInterface;
import Models.Room;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Command implementation for marking a room as unavailable for a specified date range.
 * <p>
 * This command updates the availability status of a room and sets a note explaining the reason
 * for the room being unavailable. It performs several validation checks before updating the room's
 * status and details.
 * </p>
 * <p>
 * Steps performed by this command:
 * <ol>
 *   <li>Retrieves the room by its number from the {@link HubController}.</li>
 *   <li>Parses the provided dates using the format "dd-MM-yyyy".</li>
 *   <li>Validates if the room exists and is currently available.</li>
 *   <li>Checks if the date range is valid and if the room is empty.</li>
 *   <li>Updates the room's availability status and sets the check-in and check-out dates.</li>
 *   <li>Sets a message as a note for the room.</li>
 * </ol>
 * </p>
 */
public class UnavailableCommand implements CommandInterface {
    private HubController hubController;
    private String roomNumber;
    private String dateFrom;
    private String dateTo;
    private String message;

    /**
     * Constructs an {@code UnavailableCommand} instance.
     *
     * @param hubController the {@link HubController} instance used to retrieve the room.
     * @param roomNumber    the number of the room to be marked as unavailable.
     * @param dateFrom      the start date of the unavailability period (format: "dd-MM-yyyy").
     * @param dateTo        the end date of the unavailability period (format: "dd-MM-yyyy").
     * @param message       a message to be set as a note for the room.
     */
    public UnavailableCommand(HubController hubController, String roomNumber, String dateFrom, String dateTo, String message) {
        this.hubController = hubController;
        this.roomNumber = roomNumber;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.message = message;
    }

    /**
     * Executes the command to mark a room as unavailable.
     * <p>
     * Retrieves the room by its number, parses the provided dates, and performs validation to
     * ensure the room exists, is available, and the dates are valid. If all checks pass, it updates
     * the room's status to unavailable, sets the check-in and check-out dates, and assigns the provided
     * message as a note.
     * </p>
     */
    @Override
    public void executeCommand() {
        Room foundRoom = hubController.retrieveRoomByNumber(Integer.parseInt(roomNumber));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dateFromParsed = null, dateToParsed = null;
        try {
            dateFromParsed = LocalDate.parse(this.dateFrom, formatter);
            dateToParsed = LocalDate.parse(this.dateTo, formatter);
        } catch (Exception ignored) {
        }

        if (foundRoom == null) {
            System.out.println("Room " + roomNumber + " not found!");
            return;
        }
        if (!foundRoom.getStatus()) {
            System.out.println("Room is already unavailable!");
            return;
        }
        if (!RoomDateValidators.validateDate(dateFromParsed, dateToParsed)) {
            System.out.println("Check in date cannot be after check out date and vice versa!");
            return;
        }
        if (!RoomValidators.validateRoomIsEmpty(foundRoom)) {
            System.out.println("Room is occupied!");
            return;
        }

        foundRoom.setCheckIn(dateFromParsed);
        foundRoom.setCheckOut(dateToParsed);
        foundRoom.setStatus(false);
        foundRoom.setNote(message);

        System.out.println("Room " + roomNumber + " is unavailable from " + dateFrom + " to " + dateTo + "message -> " + message);

    }
}
