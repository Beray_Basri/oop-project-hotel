package Models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Represents a singleton hub that manages a collection of rooms.
 * <p>
 * The {@code Hub} class follows the Singleton design pattern, ensuring that only one instance
 * of the hub exists throughout the application. This class provides methods to manage and
 * access a list of {@link Room} objects.
 * </p>
 * <p>
 * The class is annotated with JAXB annotations to support XML serialization and deserialization.
 * </p>
 */
@XmlRootElement(name = "hub")
public class Hub {
    private ArrayList<Room> rooms = new ArrayList<>();
    private static Hub instance;

    /**
     * Retrieves the list of rooms stored in the hub.
     * <p>
     * This method is annotated with {@code @XmlElement} to enable XML serialization.
     * </p>
     *
     * @return The list of {@link Room} objects managed by this hub.
     */
    @XmlElement(name = "room")
    public ArrayList<Room> getRoomsList() {
        return rooms;
    }

    /**
     * Retrieves or creates a single instance of the Hub class following the Singleton design pattern.
     * <p>
     * If the instance does not exist, a new instance is created. Subsequent calls will return the same instance.
     * </p>
     *
     * @return The single instance of the Hub class.
     */
    public static Hub getInstance() {
        if (instance == null) {
            instance = new Hub();
        }
        return instance;
    }


    /**
     * Clears all the data from the list of rooms in this hub.
     * <p>
     * This method removes all room entries from the internal list and prints "Data cleared".
     * </p>
     */
    public void clearRooms() {
        rooms.clear();
    }


    /**
     * Adds a room to the list of rooms in this hub.
     * <p>
     * This method appends the specified room to the internal list and prints a message indicating
     * that the room has been added.
     * </p>
     *
     * @param room The {@link Room} object to be added to the list.
     */
    public void addRoom(Room room) {
        rooms.add(room);
    }

    /**
     * Searches for a {@link Room} object by its number.
     * <p>
     * This method iterates over the list of rooms to find a match for the given room number.
     * If a matching room is found, it is returned; otherwise, {@code null} is returned.
     * </p>
     *
     * @param number The number of the room to search for.
     * @return The {@link Room} object if found, otherwise {@code null}.
     */
    public Room searchRoom(int number) {
        for (Room current_room : rooms) {
            if (current_room.getNumber() == number) {
                return current_room;
            }
        }
        return null;
    }

    /**
     * Returns a copy of the list of rooms stored in this hub.
     * <p>
     * This method creates a new {@link ArrayList} containing a copy of the rooms managed by this hub.
     * </p>
     *
     * @return A {@link ArrayList} containing the rooms in this hub.
     */
    public ArrayList<Room> returnRooms() {
        ArrayList<Room> found_rooms = new ArrayList<>();
        found_rooms.addAll(rooms);
        return found_rooms;
    }

    /**
     * Sets the list of rooms in this hub to the specified list.
     * <p>
     * This method replaces the current list of rooms with the provided list.
     * </p>
     *
     * @param roomsList The new list of {@link Room} objects to be set.
     */
    public void setRooms(ArrayList<Room> roomsList) {
        this.rooms = roomsList;
    }


}
