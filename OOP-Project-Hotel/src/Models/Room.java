package Models;

import Core.Utils.LocalDateAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Represents a Room entity in a hotel or lodging system.
 * <p>
 * The {@code Room} class holds information about a room such as its number, check-in and check-out dates,
 * number of beds, occupancy status, availability, and any additional notes.
 * <p>
 * The class is annotated with JAXB annotations to facilitate XML serialization and deserialization.
 * LocalDate fields are adapted for XML using the {@link LocalDateAdapter}.
 * </p>
 */
@XmlRootElement
public class Room {

    private int number;

    private LocalDate checkIn;

    private LocalDate checkOut;
    private int beds;
    private String note;
    private boolean status;
    private int guests;

    /**
     * Default constructor required for JAXB serialization.
     */
    public Room() {
    }

    /**
     * Constructs a Room with the specified parameters.
     *
     * @param number   The room number.
     * @param checkIn  The check-in date.
     * @param checkOut The check-out date.
     * @param beds     The number of beds in the room.
     * @param note     Additional notes about the room (e.g., maintenance, special conditions).
     * @param status   The availability status of the room (true = available, false = unavailable).
     * @param guests   The number of guests currently occupying the room.
     */
    public Room(int number, LocalDate checkIn, LocalDate checkOut, int beds, String note, boolean status, int guests) {
        this.number = number;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.beds = beds;
        this.note = note;
        this.status = status;
        this.guests = guests;
    }

    @XmlElement
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @XmlElement
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    @XmlElement
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    @XmlElement
    public int getBeds() {
        return beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    @XmlElement
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @XmlElement
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlElement
    public int getGuests() {
        return guests;
    }

    public void setGuests(int guests) {
        this.guests = guests;
    }

    @Override
    public String toString() {
        String formattedCheckIn = null;
        String formattedCheckOut = null;
        LocalDate toCheck = LocalDate.of(1000, 01, 01);
        if (this.checkIn != null && this.checkOut != null) {
            if (this.checkIn.isEqual(toCheck) && this.checkOut.isEqual(toCheck)) {
                formattedCheckIn = "Not set";
                formattedCheckOut = "Not set";
            } else {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                formattedCheckIn = checkIn.format(formatter);
                formattedCheckOut = checkOut.format(formatter);
            }
        }
        if (this.status) {
            return "Room{" +
                    "number=" + number +
                    ", checkIn=" + formattedCheckIn +
                    ", checkOut=" + formattedCheckOut +
                    ", beds=" + beds +
                    ", guests=" + guests +
                    ", note='" + note + '\'' +
                    ", status=" + "available" +
                    '}';
        } else {
            return "Room{" +
                    "number=" + number +
                    ", checkIn=" + formattedCheckIn +
                    ", checkOut=" + formattedCheckOut +
                    ", beds=" + beds +
                    ", guests=" + guests +
                    ", note='" + note + '\'' +
                    ", status=" + "unavailable" +
                    '}';
        }
    }
}
