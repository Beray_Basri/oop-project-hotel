package Views;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Models.Room;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * This class handles the presentation logic for showing available rooms in the hotel system.
 * It retrieves a list of rooms from the {@link HubController} and determines if they are available
 * on a specific date, then displays those rooms.
 */
public class AvailabilityCommandView {

    /**
     * Displays the list of rooms available on a specific date.
     * Rooms are considered available if they are either marked as "never checked in"
     * or if they are empty on the given date.
     *
     * @param hubController The controller used to interact with the hub model to retrieve room data.
     * @param date          The specific date to check for room availability.
     */
    public void showAvailableRooms(HubController hubController, LocalDate date) {
        ArrayList<Room> rooms = hubController.retrieveRoomsList();
        LocalDate toCompare = LocalDate.of(1000, 1, 1);

        for (Room room : rooms) {
            boolean comparisonResult = room.getCheckIn().equals(toCompare);
            if (comparisonResult) {
                System.out.println(room);
            } else if (RoomDateValidators.checkRoomEmptyOnGivenDate(date, room.getCheckIn(), room.getCheckOut())) {
                System.out.println(room);
            }
        }
    }
}
