package Views;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Core.Utils.Validators.RoomValidators;
import Models.Room;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * This class is responsible for displaying rooms that match a user's search criteria.
 * It retrieves the list of available rooms from the {@link HubController}, applies filtering
 * based on the number of beds, availability within a specified date range, and outputs the results.
 */
public class FindCommandView {

    /**
     * Displays the rooms that meet the search criteria of a specified date range and the required number of beds.
     * The method performs the following steps:
     * <ol>
     *     <li>Validates the check-in and check-out dates.</li>
     *     <li>Sorts the rooms by the number of beds in ascending order.</li>
     *     <li>Filters the rooms to find those that are available and have enough beds.</li>
     *     <li>Checks if the room is unoccupied or the search dates don't overlap with existing reservations.</li>
     * </ol>
     *
     * @param hubController The controller used to retrieve the list of rooms.
     * @param dateFrom      The check-in date for the search.
     * @param dateTo        The check-out date for the search.
     * @param beds          The number of beds required for the room.
     * @param emptyRoomDate A placeholder date to signify that the room is unoccupied (default used: 1000-01-01).
     */
    public void showFoundRooms(HubController hubController, LocalDate dateFrom, LocalDate dateTo, int beds, LocalDate emptyRoomDate) {
        if (!RoomDateValidators.validateDate(dateFrom, dateTo)) {
            System.out.println("Check in date cannot be after check out date and vice versa!");
            return;
        }
        ArrayList<Room> rooms = hubController.retrieveRoomsList();
        Comparator<Room> comparator = Comparator.comparingInt(Room::getBeds);
        rooms.sort(comparator);
        boolean foundRoomFlag = false;

        for (Room room : rooms) {
            if (!RoomValidators.validateEnoughBedsInRoom(beds, room)) {
                continue;
            }
            if (!room.getStatus()) {
                continue;
            }
            if (room.getCheckIn().equals(emptyRoomDate)) {
                System.out.println(room);
                foundRoomFlag = true;
                continue;
            }
            if ((dateFrom.isBefore(room.getCheckIn()) && dateTo.isBefore(room.getCheckIn())) || (dateFrom.isAfter(room.getCheckOut()) && dateTo.isAfter(room.getCheckOut()))) {
                System.out.println(room);
                foundRoomFlag = true;
                continue;
            }

            if (!foundRoomFlag) {
                System.out.println("No rooms found!");
            }
        }
    }
}




