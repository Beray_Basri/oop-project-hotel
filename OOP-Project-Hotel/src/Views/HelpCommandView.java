package Views;

/**
 * The {@code HelpCommandView} class is responsible for displaying the help information to users.
 * It provides a static method that outlines the available commands in the system, grouped into
 * file-specific and hotel-specific commands.
 */
public class HelpCommandView {

    /**
     * Displays a list of supported commands for interacting with the hotel system and file operations.
     * <p>
     * The command list is divided into two categories:
     * <ul>
     *     <li><b>File-specific commands</b>: Commands that handle file operations such as opening and closing files.</li>
     *     <li><b>Hotel system commands</b>: Commands for interacting with hotel rooms (e.g., checking in, checking availability, finding rooms).</li>
     * </ul>
     * <p>
     * This method outputs the help information directly to the console.
     */
    public static void displayHelp() {
        System.out.println("===The following commands are supported===");
        System.out.println("1. File specific commands:");
        System.out.println("-open <file>     opens <file>");
        System.out.println("-close       closes currently opened file");
        System.out.println("-help        prints this information");
        System.out.println("-exit        exits the program");
        System.out.println("\n2. Hotel system commands:");
        System.out.println("-checkin <room> <from> <to> <note> [<guests>]   register guests to a room");
        System.out.println("-availability [<date>]    list is displayed with the free rooms on given date or current date");
        System.out.println("-checkout <room>      checking out of a given room");
        System.out.println("-report <from> <to>   display a list with the free rooms for the given date");
        System.out.println("find <beds> <from> <to>  finding a suitable free room for the given period");
        System.out.println("forcefind <beds> <from> <to> urgent room find function that allows change of rooms for up to 2 rooms");
        System.out.println("unavailable <room> <from> <to> <note>  declare a room unavailable for a given period");
    }
}
