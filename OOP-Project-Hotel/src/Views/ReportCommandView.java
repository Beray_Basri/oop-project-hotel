package Views;

import Controllers.HubController;
import Core.Utils.Validators.RoomDateValidators;
import Models.Room;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * The {@code ReportCommandView} class is responsible for displaying reports of room occupancy
 * within a specified date range. The report includes details about the rooms and the duration
 * of their stay in days.
 */
public class ReportCommandView {

    /**
     * Displays a report of rooms that were occupied during the given date range.
     * <p>
     * This method performs the following steps:
     * <ol>
     *   <li>Validates that the provided check-in and check-out dates are in the correct order.</li>
     *   <li>Retrieves a list of rooms from the {@link HubController}.</li>
     *   <li>Filters rooms that were occupied during the specified date range.</li>
     *   <li>For each matching room, prints the room details and the number of days the room was occupied.</li>
     * </ol>
     *
     * @param hubController the controller responsible for managing the rooms.
     * @param dateFrom      the start date of the period to check for room occupancy.
     * @param dateTo        the end date of the period to check for room occupancy.
     */
    public void showReport(HubController hubController, LocalDate dateFrom, LocalDate dateTo) {
        if (!RoomDateValidators.validateDate(dateFrom, dateTo)) {
            System.out.println("Check in date cannot be after check out date and vice versa!");
            return;
        }
        ArrayList<Room> rooms = hubController.retrieveRoomsList();
        boolean foundRoomFlag = false;

        System.out.println("Found rooms:");
        for (Room room : rooms) {
            if (RoomDateValidators.checkRoomDates(room.getCheckIn(), room.getCheckOut(), dateFrom, dateTo)) {
                long daysStayed = ChronoUnit.DAYS.between(room.getCheckIn(), room.getCheckOut());
                System.out.println(room + " -> " + daysStayed);
                foundRoomFlag = true;
            }
        }
        if (!foundRoomFlag) {
            System.out.println("No rooms found!");
        }

    }

}
