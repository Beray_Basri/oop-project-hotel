package Views;

import Models.Room;

import java.util.ArrayList;

/**
 * The {@code RoomView} class is responsible for displaying information about rooms.
 * It provides methods to display details of all rooms or a single room.
 */
public class RoomView {

    /**
     * Displays a list of rooms by printing their details.
     * <p>
     * This method prints each room in the provided {@link ArrayList} to the console.
     *
     * @param rooms the list of rooms to display.
     */
    public void displayRooms(ArrayList<Room> rooms) {
        System.out.println("===All Rooms===");
        for (Room room : rooms) {
            System.out.println(room);
        }
    }

    /**
     * Displays the details of a single room.
     * <p>
     * This method prints the room number and details of the given {@link Room} object.
     *
     * @param room the room whose details are to be displayed.
     */
    public void displayRoom(Room room) {
        System.out.println("Room number: " + room.getNumber());
        System.out.println(room);
    }
}
